import React from "react";
import PropTypes from 'prop-types';
import './Education.scss'

class Education extends React.Component {
  render() {
      return (
          <div className="education">
            <div className="education__title">
              <h3>{this.props.name}</h3>
            </div>

      
            <section>
                {this.props.education.map(el => {
                return (
                  <article key={`${el.title}-${el.company}`}>
                    <p>Título: {el.title}</p>
                    <p>Centro: {el.company}</p>
                    <hr></hr>
                  </article>
                )
                 })}
            </section>
          </div>
        )
  };
};

Education.propTypes = {
    name: PropTypes.string.isRequired,
    elements: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        company: PropTypes.string.isRequired,
    })),
};
// De ésta forma, tipamos el contenido para que nos avise si falla algún dato
// Con.arraOf() le decimos que es un array, y con .shape() describimos el contenido del un objeto

Education.defaultProps = {
    elements: [],
    name: "Default Title",
};
// Si ponemos props por defecto, aparecerán en caso de fallar el contenido de un elemento
// No siempre vienen bien, ya que en desarrollo no nos cantará los fallos



export default Education;