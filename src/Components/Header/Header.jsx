import React from "react";
import PropTypes from "prop-types";
import "./Header.scss";
import foto from '../../foto/fotocv.jpeg'

class Header extends React.Component {
  render() {
    return (
      <div>
      <header className="header">
        <div className="header__text">
        <h1>{this.props.fullName}</h1>
        <h2>{this.props.profession}</h2>
        </div>
        <div className="header__image">
          <img
            className="header__image--img"
            src={foto}
            alt="broken"
          ></img>
        </div>  
      </header>
    </div>
    )

  }
}

Header.propTypes = {
  image: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  profession: PropTypes.string.isRequired,
};

export default Header;
