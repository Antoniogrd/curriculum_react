import React from "react";

const INITIAL_STATE = {
  title: "",
  company: "",
  description: "",
};

class JobForm extends React.Component {
  state = INITIAL_STATE;

  handleInput = (ev) => {
    const { name, value } = ev.target;

    this.setState(
      {
        [name]: value,
      },
      () => console.log(this.state)
    );
  };

  submitForm = (ev) => {
    ev.preventDefault();
    this.props.addJob(this.state);
    this.setState(INITIAL_STATE);
  };

  render() {
    return (
      <form onSubmit={this.submitForm}>
        <label>
          <p>Puesto:</p>
          <input
            type="text"
            name="title"
            value={this.state.title}
            onChange={this.handleInput}
          />
        </label>

        <label>
          <p>Empresa::</p>
          <input
            type="text"
            name="company"
            value={this.state.company}
            onChange={this.handleInput}
          />
        </label>

        <label>
          <p>Descripción:</p>
          <input
            type="text"
            name="description"
            value={this.state.description}
            onChange={this.handleInput}
          />
        </label>

        <button type="submit">Añadir trabajo</button>
      </form>
    );
  }
}

export default JobForm;
