import React from "react";

class Job extends React.Component {
  render() {
    const { title, company, description } = this.props.experience;
    return (
      <div>
        {title && <p>Trabajo: {title}</p>}
        {company && <p>Empresa: {company}</p>}
        {description && <p>Descripción: {description}</p>}
        <hr></hr>
      </div>
    );
  }
}

export default Job;
