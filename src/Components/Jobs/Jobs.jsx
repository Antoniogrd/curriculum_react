import React from "react";
// import Job from "../../Components";
// import Job from "../index"
import {JobForm, Job} from "../../Components";
import "./Jobs.scss";

class Jobs extends React.Component {
  render() {
    return (
      <div className="jobs">
        <div className="jobs__title">
          <h3>{this.props.name}</h3>
        </div>

        <section>
          {this.props.experience.map((el) => {
            return <Job key={el.title} experience={el} />;
          })}
          <JobForm addJob={this.props.addJob} />
        </section>
      </div>
    );
  }
}

export default Jobs;


