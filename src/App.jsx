import React from "react";
import "./App.scss";
// import Header from "./Components/Header";
// import Education from "./Components/Education";
// import Jobs from "./Components/Jobs";
import { Header, Education, Jobs } from './Components';

const cv = {
  firstName: "Antonio",
  lastName: "García",
  age: 34,
  image: "fotocv.jpeg",
  profession: "Programador Full Stack",
  education: [
    {
      title: "Título medio Administración",
      company: "Instituto de planificación y gestión integral",
    },
    {
      title: "FP Ténico superior en Administración y Finanzas",
      company: "IES nuevas poblaciones",
    },
    { title: "Bootcamp programador Full Stack", company: "Upgrade Hub" },
  ],
  experience: [
    {
      title: "Diversos trabajos en el sector de la madera",
      description: "Torresol, Tapisol, Tasen, Somisur...",
    },
    {
      title: "Administrativo",
      company: "Plasgen materias plásticas",
      description: "Contabilidad, tareas bancarias, atención telefónica",
    },
    {
      title: "Venta y distribución de productos alimenticios",
      company: "Autónomo",
      description:
        "Facturación, contabilidad, gestión comercial, reparto de mercancía",
    },
    {
      title: "Sector agrario/construcción",
      description: "Albañilería y temporero recogiendo aceituna",
    },
    {
      title: "Programador Java",
      company: "UST Global",
      description: "J2EE, MVC, Maven, Jsp, Mysql",
    }
  ],
};

class App extends React.Component {
  state = {
    cv: cv,
  };

  addJob = (job) => {
    this.setState({
      cv: {
        ...this.state.cv,
        experience: [...this.state.cv.experience, job],
      },
    });
  };

  render() {
    const { cv } = this.state;
    return (
      <div className="app">
        <div className="header">
          <Header
            image={cv.image}
            fullName={`${cv.firstName} ${cv.lastName}`}
            profession={cv.profession}
          />
        </div>
        <div className="info">
          <div>
            <Education name="Educación" education={cv.education} />
          </div>
          <div>
            <Jobs
              name="Experiencia"
              experience={cv.experience}
              addJob={this.addJob}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
